import { SAVE_BREADS } from "../actions/types";
  
  const defaultState = {
    breads: [],
  };
  
  export default function reducer(state = defaultState, action) {
  
    switch (action.type) {
      case SAVE_BREADS: {
          console.log("reducer bread", action.breads);
        return {
          ...state,
          breads: action.breads
        };
      }
  
      default:
        return state;
    };
  };
  