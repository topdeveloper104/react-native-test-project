import { SAVE_BREADS } from "./types";

  export const saveBreads = (breads) => ({
    type: SAVE_BREADS,
    breads
  });
    
  export const get_breads = () => {
    return (dispatch, getState) => {
      fetch("https://dog.ceo/api/breeds/list/all", {
        method: "GET"
      }).then(response => {
        response.json()
          .then(responseJSON => {
            console.log('all breads = ', responseJSON.message);
            dispatch(saveBreads(responseJSON.message));
          })
      })
    };
  };
  