import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";
import dogReducer from "./reducers/dog";
import breadsReducer from "./reducers/breads";

/**
 * Logs all actions and states after they are dispatched.
 */
const logger = store => next => action => {
  console.info("dispatching", action);
  let result = next(action);
  console.log("next state", store.getState());
  return result;
};

const store = createStore(
  combineReducers({
    dog: dogReducer,
    breads: breadsReducer
  }),
  applyMiddleware(thunk, logger)
);

export default store;
