import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import { connect } from "react-redux";
import { get_breads } from "../../actions/breads";

class Main extends Component {

	componentWillMount(props) {
    this.props.dispatch(get_breads());
  }

  onPressDog = (name) => {
    this.props.navigator.push({
      screen: 'inkind.test.Detail',
      title: 'Detail Screen',
      passProps: { dogBread: name }
    })
  }

  render() {
    var bread_array = [];
    for (var key in this.props.breads) {
      if (this.props.breads[key].length === 0) {
        bread_array.push(key);
      } else {
        this.props.breads[key].map((value, index) => {
          bread_array.push(key + '-' + value);
        })
      }
    }
    return (
      <View style={{paddingTop: 50, paddingLeft: 15, paddingRight: 15}}>
        <Text>All Breads</Text>
        <ScrollView style={{marginTop: 30}}>
          {
            bread_array.map((value, index) => {
              return (
                <TouchableOpacity key={index} style={{width: '100%', height: 20, borderBottomColor: 'black', borderBottomWidth: 1}} onPress={() => this.onPressDog(value)}>
                  <Text>{value}</Text>
                </TouchableOpacity>
              )
            })
          }
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
	return {
    breads: state.breads.breads
	};
};

export default connect(mapStateToProps)(Main);
