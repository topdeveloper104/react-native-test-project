import { Navigation } from 'react-native-navigation';
import Main from "./Main";
import Detail from "./Detail";

export function register_screens(store, Provider) {

  Navigation.registerComponent(
    'inkind.test.Main',
    () => Main,
    store,
    Provider
  );

  Navigation.registerComponent(
    'inkind.test.Detail',
    () => Detail,
    store,
    Provider
  );

};
